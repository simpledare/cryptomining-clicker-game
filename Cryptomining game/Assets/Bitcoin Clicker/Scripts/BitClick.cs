﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Advertisements;

public class BitClick : MonoBehaviour {

	public GameObject bitcoin, gtx1060, gtx1080, asic, new_card;
	private GameObject[] card = new GameObject[8];
	public GameObject set1, set2, set3, set4, conf1, conf2, conf3, conf4, forest, city, blue, bigcity;
	public Text balance, balance1, balancedol, balancedol1, balancedol2;
	private double score = 0f, dol;
	private int touches = 0;
	public AudioSource click;
	private int advclick;
	private bool isSeen = false;
	private GameObject[] bitposit = new GameObject[8];
	private float gtx1060earn = 0.0001f; // HOW MUCH MINE GTX1060
	private float gtx1080earn = 0.0003f; // HOW MUCH MINE GTX1080
	private float asicearn = 0.0005f; // HOW MUCH MINE ASIC
	private float new_cardearn = 0.0005f; // HOW MUCH MINE NEW CARD

	void Start()
	{
		LoadGame();
		InvokeRepeating("EverySecond", 0f, 1.0f);
		
		if(Advertisement.isSupported) Advertisement.Initialize("1523431");

		Debug.Log("Ready");
		advclick = 0;
	}

	void OnMouseUpAsButton()
	{
		click.Play();
		PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + 0.0001f);
		score = (double)PlayerPrefs.GetFloat("balbit");
		score = System.Math.Round(score, 4, MidpointRounding.AwayFromZero);
		PlayerPrefs.SetFloat("balbit", (float)score);
		balance.text = PlayerPrefs.GetFloat("balbit") + " BTC";
		balance1.text = PlayerPrefs.GetFloat("balbit") + " BTC";
		advclick++;
	}

	void OnMouseDown()
	{
		bitcoin.transform.localScale = new Vector3(bitcoin.transform.localScale.x - 0.3f, bitcoin.transform.localScale.y - 0.3f, bitcoin.transform.localScale.z);
	}

	void OnMouseUp()
	{
		bitcoin.transform.localScale = new Vector3(bitcoin.transform.localScale.x + 0.3f, bitcoin.transform.localScale.y + 0.3f, bitcoin.transform.localScale.z);
	}

	void Update()
	{
		score = (double)PlayerPrefs.GetFloat("balbit");
		score = System.Math.Round(score, 4, MidpointRounding.AwayFromZero);
		PlayerPrefs.SetFloat("balbit", (float)score);
		//for(int i = 0; i < bitposit.Length; i++) bitposit[i] = gtx1060.GetComponent<Cards>().posit[i];
		
		dol = (double)PlayerPrefs.GetFloat("baldol");
		dol = System.Math.Round(dol, 2, MidpointRounding.AwayFromZero);
		PlayerPrefs.SetFloat("baldol", (float)dol);
		balance.text = PlayerPrefs.GetFloat("balbit") + " ETH";
		balance1.text = PlayerPrefs.GetFloat("balbit") + " ETH";
		balancedol.text = PlayerPrefs.GetFloat("baldol") + " USD";
		balancedol1.text = PlayerPrefs.GetFloat("baldol") + " USD";
		balancedol2.text = PlayerPrefs.GetFloat("baldol") + " USD";

		if(Input.GetKeyDown(KeyCode.W))
		{
			Debug.Log("rebooted:)");
			PlayerPrefs.SetFloat("balbit", 0f);
			PlayerPrefs.SetFloat("baldol", 0f);
			score = 0;
		}

		if(Input.GetKeyDown(KeyCode.E))
		{
			Debug.Log("boosted:)");
			PlayerPrefs.SetFloat("baldol", 100000f);
		}

		if(Input.GetKeyDown(KeyCode.Q))
		{
			Debug.Log("rest");
			PlayerPrefs.SetFloat("ib2", 0f);
			PlayerPrefs.SetFloat("ib3", 0f);
			PlayerPrefs.SetFloat("ib4", 0f);
			PlayerPrefs.SetFloat("zan1", 0f);
			PlayerPrefs.SetFloat("zan2", 0f);
			PlayerPrefs.SetFloat("zan3", 0f);
			PlayerPrefs.SetFloat("zan4", 0f);
			PlayerPrefs.SetFloat("zan5", 0f);
			PlayerPrefs.SetFloat("zan6", 0f);
			PlayerPrefs.SetFloat("zan7", 0f);
			PlayerPrefs.SetFloat("zan8", 0f);
			Debug.Log("rest");
		}

		if(advclick == 100)
		{
			isSeen = false;
			Debug.Log("Ready");
		} 

		if(advclick > 180)
		{
			advclick = 0;
			Debug.Log("See");
			isSeen = false;
			if(Advertisement.IsReady())
			{
				Advertisement.Show();
			}
		} 
		if(PlayerPrefs.GetFloat("zan1") == 0) Destroy(card[0]);
		if(PlayerPrefs.GetFloat("zan2") == 0) Destroy(card[1]);
		if(PlayerPrefs.GetFloat("zan3") == 0) Destroy(card[2]);
		if(PlayerPrefs.GetFloat("zan4") == 0) Destroy(card[3]);
		if(PlayerPrefs.GetFloat("zan5") == 0) Destroy(card[4]);
		if(PlayerPrefs.GetFloat("zan6") == 0) Destroy(card[5]);
		if(PlayerPrefs.GetFloat("zan7") == 0) Destroy(card[6]);
		if(PlayerPrefs.GetFloat("zan8") == 0) Destroy(card[7]);
	}

	void EverySecond()
	{
		if(PlayerPrefs.GetFloat("call") == 1f)
		{
			LoadGame();
			PlayerPrefs.SetFloat("call", 0f);
		}

		if(PlayerPrefs.GetFloat("zan1") == 1.1f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1060earn);
		if(PlayerPrefs.GetFloat("zan2") == 1.1f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1060earn);
		if(PlayerPrefs.GetFloat("zan3") == 1.1f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1060earn);
		if(PlayerPrefs.GetFloat("zan4") == 1.1f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1060earn);
		if(PlayerPrefs.GetFloat("zan5") == 1.1f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1060earn);
		if(PlayerPrefs.GetFloat("zan6") == 1.1f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1060earn);
		if(PlayerPrefs.GetFloat("zan7") == 1.1f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1060earn);
		if(PlayerPrefs.GetFloat("zan8") == 1.1f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1060earn);

		if(PlayerPrefs.GetFloat("zan1") == 1.2f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1080earn);
		if(PlayerPrefs.GetFloat("zan2") == 1.2f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1080earn);
		if(PlayerPrefs.GetFloat("zan3") == 1.2f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1080earn);
		if(PlayerPrefs.GetFloat("zan4") == 1.2f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1080earn);
		if(PlayerPrefs.GetFloat("zan5") == 1.2f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1080earn);
		if(PlayerPrefs.GetFloat("zan6") == 1.2f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1080earn);
		if(PlayerPrefs.GetFloat("zan7") == 1.2f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1080earn);
		if(PlayerPrefs.GetFloat("zan8") == 1.2f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + gtx1080earn);

		if(PlayerPrefs.GetFloat("zan1") == 1.3f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + asicearn);
		if(PlayerPrefs.GetFloat("zan2") == 1.3f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + asicearn);
		if(PlayerPrefs.GetFloat("zan3") == 1.3f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + asicearn);
		if(PlayerPrefs.GetFloat("zan4") == 1.3f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + asicearn);
		if(PlayerPrefs.GetFloat("zan5") == 1.3f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + asicearn);
		if(PlayerPrefs.GetFloat("zan6") == 1.3f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + asicearn);
		if(PlayerPrefs.GetFloat("zan7") == 1.3f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + asicearn);
		if(PlayerPrefs.GetFloat("zan8") == 1.3f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + asicearn);

		if(PlayerPrefs.GetFloat("zan1") == 1.4f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + new_cardearn);
		if(PlayerPrefs.GetFloat("zan2") == 1.4f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + new_cardearn);
		if(PlayerPrefs.GetFloat("zan3") == 1.4f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + new_cardearn);
		if(PlayerPrefs.GetFloat("zan4") == 1.4f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + new_cardearn);
		if(PlayerPrefs.GetFloat("zan5") == 1.4f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + new_cardearn);
		if(PlayerPrefs.GetFloat("zan6") == 1.4f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + new_cardearn);
		if(PlayerPrefs.GetFloat("zan7") == 1.4f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + new_cardearn);
		if(PlayerPrefs.GetFloat("zan8") == 1.4f) PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") + new_cardearn);
	}

	public void LoadGame()
	{
		score = (double)PlayerPrefs.GetFloat("balbit");
		score = System.Math.Round(score, 4, MidpointRounding.AwayFromZero);
		balance.text = score + " BTC";
		balance1.text = score + " BTC";
		Destroy(card[1]);
		
		

		if(PlayerPrefs.GetFloat("zan1") == 1.1f) card[0] = Instantiate(gtx1060, new Vector3(-1.1f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan2") == 1.1f) card[1] = Instantiate(gtx1060, new Vector3(0f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan3") == 1.1f) card[2] = Instantiate(gtx1060, new Vector3(1.1f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan4") == 1.1f) card[3] = Instantiate(gtx1060, new Vector3(2.2f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan5") == 1.1f) card[4] = Instantiate(gtx1060, new Vector3(-1.1f, -2.15f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan6") == 1.1f) card[5] = Instantiate(gtx1060, new Vector3(0f, -2.15f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan7") == 1.1f) card[6] = Instantiate(gtx1060, new Vector3(1.1f, -2.15f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan8") == 1.1f) card[7] = Instantiate(gtx1060, new Vector3(2.2f, -2.15f, 19f), Quaternion.identity);

		if(PlayerPrefs.GetFloat("zan1") == 1.2f) card[0] = Instantiate(gtx1080, new Vector3(-1.1f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan2") == 1.2f) card[1] = Instantiate(gtx1080, new Vector3(0f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan3") == 1.2f) card[2] = Instantiate(gtx1080, new Vector3(1.1f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan4") == 1.2f) card[3] = Instantiate(gtx1080, new Vector3(2.2f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan5") == 1.2f) card[4] = Instantiate(gtx1080, new Vector3(-1.1f, -2.15f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan6") == 1.2f) card[5] = Instantiate(gtx1080, new Vector3(0f, -2.15f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan7") == 1.2f) card[6] = Instantiate(gtx1080, new Vector3(1.1f, -2.15f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan8") == 1.2f) card[7] = Instantiate(gtx1080, new Vector3(2.2f, -2.15f, 19f), Quaternion.identity);

		if(PlayerPrefs.GetFloat("zan1") == 1.3f) card[0] = Instantiate(asic, new Vector3(-1.1f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan2") == 1.3f) card[1] = Instantiate(asic, new Vector3(0f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan3") == 1.3f) card[2] = Instantiate(asic, new Vector3(1.1f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan4") == 1.3f) card[3] = Instantiate(asic, new Vector3(2.2f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan5") == 1.3f) card[4] = Instantiate(asic, new Vector3(-1.1f, -2.15f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan6") == 1.3f) card[5] = Instantiate(asic, new Vector3(0f, -2.15f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan7") == 1.3f) card[6] = Instantiate(asic, new Vector3(1.1f, -2.15f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan8") == 1.3f) card[7] = Instantiate(asic, new Vector3(2.2f, -2.15f, 19f), Quaternion.identity);

		if(PlayerPrefs.GetFloat("zan1") == 1.4f) card[0] = Instantiate(new_card, new Vector3(-1.1f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan2") == 1.4f) card[1] = Instantiate(new_card, new Vector3(0f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan3") == 1.4f) card[2] = Instantiate(new_card, new Vector3(1.1f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan4") == 1.4f) card[3] = Instantiate(new_card, new Vector3(2.2f, -1.05f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan5") == 1.4f) card[4] = Instantiate(new_card, new Vector3(-1.1f, -2.15f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan6") == 1.4f) card[5] = Instantiate(new_card, new Vector3(0f, -2.15f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan7") == 1.4f) card[6] = Instantiate(new_card, new Vector3(1.1f, -2.15f, 19f), Quaternion.identity);
		if(PlayerPrefs.GetFloat("zan8") == 1.4f) card[7] = Instantiate(new_card, new Vector3(2.2f, -2.15f, 19f), Quaternion.identity);

		if(PlayerPrefs.GetFloat("is1") == 1)
		{
			set2.gameObject.SetActive(true);
			set3.gameObject.SetActive(true);
			set4.gameObject.SetActive(true);

			set1.gameObject.SetActive(false);

			conf2.gameObject.SetActive(false);
			conf3.gameObject.SetActive(false);
			conf4.gameObject.SetActive(false);

			conf1.gameObject.SetActive(true);

			bigcity.gameObject.SetActive(false);
			blue.gameObject.SetActive(false);
			city.gameObject.SetActive(false);

			forest.gameObject.SetActive(true);
		}

		if(PlayerPrefs.GetFloat("is2") == 1f)
		{
			set1.gameObject.SetActive(true);
			set3.gameObject.SetActive(true);
			set4.gameObject.SetActive(true);

			set2.gameObject.SetActive(false);

			forest.gameObject.SetActive(false);
			bigcity.gameObject.SetActive(false);
			blue.gameObject.SetActive(false);
				
			city.gameObject.SetActive(true);
				
			conf1.gameObject.SetActive(false);
			conf3.gameObject.SetActive(false);
			conf4.gameObject.SetActive(false);

			conf2.gameObject.SetActive(true);
		}

		if(PlayerPrefs.GetFloat("is3") == 1f)
		{
			set1.gameObject.SetActive(true);
			set2.gameObject.SetActive(true);
			set4.gameObject.SetActive(true);

			set3.gameObject.SetActive(false);

			forest.gameObject.SetActive(false);
			bigcity.gameObject.SetActive(false);
			city.gameObject.SetActive(false);
				
			blue.gameObject.SetActive(true);
				
			conf1.gameObject.SetActive(false);
			conf2.gameObject.SetActive(false);
			conf4.gameObject.SetActive(false);

			conf3.gameObject.SetActive(true);
		}

		if(PlayerPrefs.GetFloat("is4") == 1f)
		{
			set1.gameObject.SetActive(true);
			set2.gameObject.SetActive(true);
			set3.gameObject.SetActive(true);

			set4.gameObject.SetActive(false);

			forest.gameObject.SetActive(false);
			blue.gameObject.SetActive(false);
			city.gameObject.SetActive(false);
				
			bigcity.gameObject.SetActive(true);
				
			conf1.gameObject.SetActive(false);
			conf2.gameObject.SetActive(false);
			conf3.gameObject.SetActive(false);

			conf4.gameObject.SetActive(true);
		}
	}
}
