﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Buttons : MonoBehaviour {

	public Canvas canvasEx, canvasShop, canvasSet;
	public AudioSource click, shop, buy, music;
	public GameObject[] cards = new GameObject[4];
	void OnMouseDown()
	{
		switch(gameObject.name)
		{
			case "Shop2":
			transform.localScale = new Vector3(transform.localScale.x - 0.5f, transform.localScale.y - 0.5f, transform.localScale.z);
				break;

			case "Exchange2":
			transform.localScale = new Vector3(transform.localScale.x - 0.5f, transform.localScale.y - 0.5f, transform.localScale.z);
				break;

			case "Settings":
			transform.localScale = new Vector3(transform.localScale.x - 0.5f, transform.localScale.y - 0.5f, transform.localScale.z);
				break;
		}
	}

	void OnMouseUp()
	{
		switch(gameObject.name)
		{
			case "Shop2":
			transform.localScale = new Vector3(transform.localScale.x + 0.5f, transform.localScale.y + 0.5f, transform.localScale.z);
				break;

			case "Exchange2":
			transform.localScale = new Vector3(transform.localScale.x + 0.5f, transform.localScale.y + 0.5f, transform.localScale.z);
				break;

			case "Settings":
			transform.localScale = new Vector3(transform.localScale.x + 0.5f, transform.localScale.y + 0.5f, transform.localScale.z);
				break;
		}
	}

	void OnMouseUpAsButton()
	{
		switch(gameObject.name)
		{
			case "Shop2":
			click.Play();
			for(int i = 0; i < cards.Length; i++) cards[i].gameObject.SetActive(true);
			canvasShop.gameObject.SetActive(true);
				break;

			case "Exchange2":
			music.Stop();
			click.Play();
			if(PlayerPrefs.GetFloat("isMuted") != 1f) shop.Play();
			buy.Play();
			canvasEx.gameObject.SetActive(true);
				break;

			case "Settings":
			click.Play();
			canvasSet.gameObject.SetActive(true);
				break;
		}
	}
}
