﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Exchange : MonoBehaviour {
	
	public Image close, enter;
	public Text btc, usdcomplete, error, cour;
	public Canvas canvasex;
	private float bitcoin, usd;
	private float bitcoin_course = 6000f;
	private double _usd;
	public AudioSource buy, click, music, shop;


	void Start()
	{
		cour.text = "1 ETH = " + bitcoin_course + " USD";
	}
	void Update()
	{
		try
		{
			float.TryParse(btc.text, out bitcoin);

			if(PlayerPrefs.GetFloat("balbit") < bitcoin)
			{
				
			}

			if(PlayerPrefs.GetFloat("balbit") >= bitcoin)
			{
				error.text = "";
				usdcomplete.text = "" + (bitcoin * bitcoin_course);
			}
		}

		catch
		{
			Debug.Log("ERROR");
		}
	}
	void OnMouseDown()
	{
		switch(gameObject.name)
		{
			case "Close":
			close.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
				break;
			
			case "enter":
			enter.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
				break;
		}
	}

	void OnMouseUp()
	{
		switch(gameObject.name)
		{
			case "Close":
			close.transform.localScale = new Vector3(1f, 1f, 1f);
				break;

			case "enter":
			enter.transform.localScale = new Vector3(1f, 1f, 1f);
				break;
		}
	}

	void OnMouseUpAsButton()
	{
		switch(gameObject.name)
		{
			case "Close":
			click.Play();
			shop.Stop();
			if(PlayerPrefs.GetFloat("isMuted") != 1f) music.Play();
			error.text = "";
			close.transform.localScale = new Vector3(1f, 1f, 1f);
			canvasex.gameObject.SetActive(false);
				break;

			case "enter":
			if(PlayerPrefs.GetFloat("balbit") < bitcoin)
			{
				error.text = "ERROR";
			}

			if(PlayerPrefs.GetFloat("balbit") >= bitcoin)
			{
				buy.Play();
				PlayerPrefs.SetFloat("balbit", PlayerPrefs.GetFloat("balbit") - bitcoin);
				float.TryParse(usdcomplete.text, out usd);
				_usd = (double)usd;
				_usd = System.Math.Round(_usd, 2, MidpointRounding.AwayFromZero);
				usd = (float)_usd;
				PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") + usd);
				error.text = "Exchanged!";
			}
				break;
		}
	}
}
