﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour {

	public AudioSource click, music, shopmusic;
	public Canvas canvasSet;
	public Image mus, mus1;

	void OnMouseDown()
	{
		switch(gameObject.name)
		{
			case "Close":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.1f, gameObject.transform.localScale.y - 0.1f, gameObject.transform.localScale.z);
				break;

			case "Music":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.1f, gameObject.transform.localScale.y - 0.1f, gameObject.transform.localScale.z);
				break;
		}
	}

	void OnMouseUp()
	{
		switch(gameObject.name)
		{
			case "Close":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.1f, gameObject.transform.localScale.y + 0.1f, gameObject.transform.localScale.z);
				break;

			case "Music":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.1f, gameObject.transform.localScale.y + 0.1f, gameObject.transform.localScale.z);
				break;
		}
	}

	void OnMouseUpAsButton()
	{
		switch(gameObject.name)
		{
			case "Close":
			click.Play();
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.1f, gameObject.transform.localScale.y + 0.1f, gameObject.transform.localScale.z);
			canvasSet.gameObject.SetActive(false);
				break;

			case "Music":
			click.Play();
			if(PlayerPrefs.GetFloat("isMuted") == 0f)
			{
				music.Stop();
				PlayerPrefs.SetFloat("isMuted", 1f);
				mus1.gameObject.SetActive(true);
				return;
			}
			if(PlayerPrefs.GetFloat("isMuted") == 1f)
			{
				music.Play();
				PlayerPrefs.SetFloat("isMuted", 0f);
				mus1.gameObject.SetActive(false);
				return;
			}
				break;

		}
	}
}
