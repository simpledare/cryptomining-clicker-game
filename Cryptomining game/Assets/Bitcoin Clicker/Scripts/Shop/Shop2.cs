﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Shop2 : MonoBehaviour {

	public GameObject grapicscardssection, wallpapersection, sellsection;
	public GameObject[] cards = new GameObject[4];
	
	public Text inf;
	public Image backmain, back, back1;
	public AudioSource click, music, shop;
	private int i;

	void Start()
	{
		click.Play();
		inf.text = "";
		grapicscardssection.gameObject.SetActive(true);
		wallpapersection.gameObject.SetActive(false);
		for(i = 0; i < cards.Length; i++) cards[i].gameObject.SetActive(true);
			
		backmain.gameObject.SetActive(true);
		back.gameObject.SetActive(false);
		back1.gameObject.SetActive(false);
	}
	void OnMouseDown()
	{
		switch(gameObject.name)
		{
			case "sel_grap":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.1f, gameObject.transform.localScale.y - 0.1f, gameObject.transform.localScale.z);
				break;
			
			case "sel_wall":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.1f, gameObject.transform.localScale.y - 0.1f, gameObject.transform.localScale.z);
				break;

			case "sell_cards":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.1f, gameObject.transform.localScale.y - 0.1f, gameObject.transform.localScale.z);
				break;
		}
	}

	void OnMouseUp()
	{
		switch(gameObject.name)
		{
			case "sel_grap":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.1f, gameObject.transform.localScale.y + 0.1f, gameObject.transform.localScale.z);
				break;
			
			case "sel_wall":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.1f, gameObject.transform.localScale.y + 0.1f, gameObject.transform.localScale.z);
				break;

			case "sell_cards":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.1f, gameObject.transform.localScale.y + 0.1f, gameObject.transform.localScale.z);
				break;
		}
	}

	void OnMouseUpAsButton()
	{
		switch(gameObject.name)
		{
			case "Close":
			click.Play();
			inf.text = "";
			grapicscardssection.gameObject.SetActive(true);
			wallpapersection.gameObject.SetActive(false);
			sellsection.gameObject.SetActive(false);
			for(i = 0; i < cards.Length; i++) cards[i].gameObject.SetActive(false);
			backmain.gameObject.SetActive(true);
			back.gameObject.SetActive(false);
			back1.gameObject.SetActive(false);
				break;

			case "sel_grap":
			click.Play();
			inf.text = "";
			grapicscardssection.gameObject.SetActive(true);
			wallpapersection.gameObject.SetActive(false);
			sellsection.gameObject.SetActive(false);
			for(i = 0; i < cards.Length; i++) cards[i].gameObject.SetActive(true);
			backmain.gameObject.SetActive(true);
			back.gameObject.SetActive(false);
			back1.gameObject.SetActive(false);
				break;
			
			

			case "sel_wall":
			click.Play();
			inf.text = "";
			grapicscardssection.gameObject.SetActive(false);
			wallpapersection.gameObject.SetActive(true);
			for(i = 0; i < cards.Length; i++) cards[i].gameObject.SetActive(false);
			sellsection.gameObject.SetActive(false);
			backmain.gameObject.SetActive(true);
			back.gameObject.SetActive(false);
			back1.gameObject.SetActive(false);
				break;

			case "sell_cards":
			click.Play();
			inf.text = "";
			grapicscardssection.gameObject.SetActive(false);
			wallpapersection.gameObject.SetActive(false);
			sellsection.gameObject.SetActive(true);
			for(i = 0; i < cards.Length; i++) cards[i].gameObject.SetActive(false);
			backmain.gameObject.SetActive(false);
			back.gameObject.SetActive(true);
			back1.gameObject.SetActive(true);
			break;

			case "Ad":
			
				break;
		}
	}
}
