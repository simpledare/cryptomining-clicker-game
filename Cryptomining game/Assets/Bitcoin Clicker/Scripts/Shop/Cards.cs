﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cards : MonoBehaviour {

	public AudioSource click, buy;
	public Text inf, price1, price2, price3, price4;
	public GameObject gtx1060, gtx1080, asic, new_card;
	private int gtx1060price = 499; // PRICE OF GTX1060
	private int gtx1080price = 2499; // PRICE OF GTX1080
	private int asicprice = 9998; // PRICE OF ASIC
	private int newcardprice = 9997; // PRICE OF NEW CARD
	private GameObject[] posit = new GameObject[8];
	private static int firstload = 0;
	public GameObject mainscript;

	void Start()
	{
		price1.text = gtx1060price + " $";
		price2.text = gtx1080price + " $";
		price3.text = asicprice + " $";
		price4.text = newcardprice + " $";
		if(firstload == 0)
		{
			LoadCards();
			firstload++;
		}
		
	}

	public void LoadCards()
	{
		if(PlayerPrefs.GetFloat("zan1") == 1.1f) posit[0] = GameObject.Find("gtx1060(Clone)");
		if(PlayerPrefs.GetFloat("zan2") == 1.1f) posit[1] = GameObject.Find("gtx1060(Clone)");
		if(PlayerPrefs.GetFloat("zan3") == 1.1f) posit[2] = GameObject.Find("gtx1060(Clone)");
		if(PlayerPrefs.GetFloat("zan4") == 1.1f) posit[3] = GameObject.Find("gtx1060(Clone)");
		if(PlayerPrefs.GetFloat("zan5") == 1.1f) posit[4] = GameObject.Find("gtx1060(Clone)");
		if(PlayerPrefs.GetFloat("zan6") == 1.1f) posit[5] = GameObject.Find("gtx1060(Clone)"); 
		if(PlayerPrefs.GetFloat("zan7") == 1.1f) posit[6] = GameObject.Find("gtx1060(Clone)");
		if(PlayerPrefs.GetFloat("zan8") == 1.1f) posit[7] = GameObject.Find("gtx1060(Clone)");

		if(PlayerPrefs.GetFloat("zan1") == 1.2f) posit[0] = GameObject.Find("gtx1080(Clone)");
		if(PlayerPrefs.GetFloat("zan2") == 1.2f) posit[1] = GameObject.Find("gtx1080(Clone)");
		if(PlayerPrefs.GetFloat("zan3") == 1.2f) posit[2] = GameObject.Find("gtx1080(Clone)");
		if(PlayerPrefs.GetFloat("zan4") == 1.2f) posit[3] = GameObject.Find("gtx1080(Clone)");
		if(PlayerPrefs.GetFloat("zan5") == 1.2f) posit[4] = GameObject.Find("gtx1080(Clone)");
		if(PlayerPrefs.GetFloat("zan6") == 1.2f) posit[5] = GameObject.Find("gtx1080(Clone)");
		if(PlayerPrefs.GetFloat("zan7") == 1.2f) posit[6] = GameObject.Find("gtx1080(Clone)");
		if(PlayerPrefs.GetFloat("zan8") == 1.2f) posit[7] = GameObject.Find("gtx1080(Clone)");

		if(PlayerPrefs.GetFloat("zan1") == 1.3f) posit[0] = GameObject.Find("asic(Clone)");
		if(PlayerPrefs.GetFloat("zan2") == 1.3f) posit[1] = GameObject.Find("asic(Clone)");
		if(PlayerPrefs.GetFloat("zan3") == 1.3f) posit[2] = GameObject.Find("asic(Clone)");
		if(PlayerPrefs.GetFloat("zan4") == 1.3f) posit[3] = GameObject.Find("asic(Clone)");
		if(PlayerPrefs.GetFloat("zan5") == 1.3f) posit[4] = GameObject.Find("asic(Clone)");
		if(PlayerPrefs.GetFloat("zan6") == 1.3f) posit[5] = GameObject.Find("asic(Clone)");
		if(PlayerPrefs.GetFloat("zan7") == 1.3f) posit[6] = GameObject.Find("asic(Clone)");
		if(PlayerPrefs.GetFloat("zan8") == 1.3f) posit[7] = GameObject.Find("asic(Clone)");

		if(PlayerPrefs.GetFloat("zan1") == 1.4f) posit[0] = GameObject.Find("new card(Clone)");
		if(PlayerPrefs.GetFloat("zan2") == 1.4f) posit[1] = GameObject.Find("new card(Clone)");
		if(PlayerPrefs.GetFloat("zan3") == 1.4f) posit[2] = GameObject.Find("new card(Clone)");
		if(PlayerPrefs.GetFloat("zan4") == 1.4f) posit[3] = GameObject.Find("new card(Clone)");
		if(PlayerPrefs.GetFloat("zan5") == 1.4f) posit[4] = GameObject.Find("new card(Clone)");
		if(PlayerPrefs.GetFloat("zan6") == 1.4f) posit[5] = GameObject.Find("new card(Clone)");
		if(PlayerPrefs.GetFloat("zan7") == 1.4f) posit[6] = GameObject.Find("new card(Clone)");
		if(PlayerPrefs.GetFloat("zan8") == 1.4f) posit[7] = GameObject.Find("new card(Clone)");
	}

	void OnMouseDown()
	{
		switch(gameObject.name)
		{
			case"Select1card":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.1f, gameObject.transform.localScale.y - 0.1f, gameObject.transform.localScale.z);
				break;

			case"Select2card":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.1f, gameObject.transform.localScale.y - 0.1f, gameObject.transform.localScale.z);
				break;

			case"Select3card":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.1f, gameObject.transform.localScale.y - 0.1f, gameObject.transform.localScale.z);
				break;

			case"Select4card":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.1f, gameObject.transform.localScale.y - 0.1f, gameObject.transform.localScale.z);
				break;

			case"double_card 1060(Clone)":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.05f, gameObject.transform.localScale.y - 0.05f, gameObject.transform.localScale.z);
				break;

			case"double_card(Clone)":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.1f, gameObject.transform.localScale.y - 0.1f, gameObject.transform.localScale.z);
				break;

			case"asic(Clone)":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - 0.4f, gameObject.transform.localScale.y - 0.4f, gameObject.transform.localScale.z);
				break;
		}
	}

	void OnMouseUp()
	{
		switch(gameObject.name)
		{
			case"Select1card":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.1f, gameObject.transform.localScale.y + 0.1f, gameObject.transform.localScale.z);
				break;

			case"Select2card":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.1f, gameObject.transform.localScale.y + 0.1f, gameObject.transform.localScale.z);
				break;

			case"Select3card":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.1f, gameObject.transform.localScale.y + 0.1f, gameObject.transform.localScale.z);
				break;

			case"Select4card":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.1f, gameObject.transform.localScale.y + 0.1f, gameObject.transform.localScale.z);
				break;	

			case"double_card 1060(Clone)":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.05f, gameObject.transform.localScale.y + 0.05f, gameObject.transform.localScale.z);
				break;

			case"double_card(Clone)":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.1f, gameObject.transform.localScale.y + 0.1f, gameObject.transform.localScale.z);
				break;

			case"asic(Clone)":
			gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.4f, gameObject.transform.localScale.y + 0.4f, gameObject.transform.localScale.z);
				break;
		}
	}

	void OnMouseUpAsButton()
	{
		switch(gameObject.name)
		{
			case"Select1card":
			click.Play();

			if(PlayerPrefs.GetFloat("baldol") < gtx1060price) // PRICE OF 1 CARD
			{
				inf.text = "You have no money!";
			}
			else
			{
				if(PlayerPrefs.GetFloat("zan1") != 1.1f && PlayerPrefs.GetFloat("zan1") != 1.2f && PlayerPrefs.GetFloat("zan1") != 1.3f && PlayerPrefs.GetFloat("zan1") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1060price);  // PRICE OF 1 CARD
					posit[0] = Instantiate(gtx1060, new Vector3(-1.1f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan1", 1.1f);
					return;
					
				}
				if(PlayerPrefs.GetFloat("zan2") != 1.1f && PlayerPrefs.GetFloat("zan2") != 1.2f && PlayerPrefs.GetFloat("zan2") != 1.3f && PlayerPrefs.GetFloat("zan2") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1060price);  // PRICE OF 1 CARD
					posit[1] = Instantiate(gtx1060, new Vector3(0f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan2", 1.1f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan3") != 1.1f && PlayerPrefs.GetFloat("zan3") != 1.2f && PlayerPrefs.GetFloat("zan3") != 1.3f && PlayerPrefs.GetFloat("zan3") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1060price);  // PRICE OF 1 CARD
					posit[2] = Instantiate(gtx1060, new Vector3(1.1f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan3", 1.1f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan4") != 1.1f && PlayerPrefs.GetFloat("zan4") != 1.2f && PlayerPrefs.GetFloat("zan4") != 1.3f && PlayerPrefs.GetFloat("zan4") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1060price);  // PRICE OF 1 CARD
					posit[3] = Instantiate(gtx1060, new Vector3(2.2f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan4", 1.1f);
					
					return;
				}
				// VTOROY RYAD

				if(PlayerPrefs.GetFloat("zan5") != 1.1f && PlayerPrefs.GetFloat("zan5") != 1.2f && PlayerPrefs.GetFloat("zan5") != 1.3f && PlayerPrefs.GetFloat("zan5") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1060price);  // PRICE OF 1 CARD
					posit[4] = Instantiate(gtx1060, new Vector3(-1.1f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan5", 1.1f);
					
					return;
					
				}
				if(PlayerPrefs.GetFloat("zan6") != 1.1f && PlayerPrefs.GetFloat("zan6") != 1.2f && PlayerPrefs.GetFloat("zan6") != 1.3f && PlayerPrefs.GetFloat("zan6") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1060price);  // PRICE OF 1 CARD
					posit[5] = Instantiate(gtx1060, new Vector3(0f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan6", 1.1f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan7") != 1.1f && PlayerPrefs.GetFloat("zan7") != 1.2f && PlayerPrefs.GetFloat("zan7") != 1.3f && PlayerPrefs.GetFloat("zan7") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1060price);  // PRICE OF 1 CARD
					posit[6] = Instantiate(gtx1060, new Vector3(1.1f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan7", 1.1f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan8") != 1.1f && PlayerPrefs.GetFloat("zan8") != 1.2f && PlayerPrefs.GetFloat("zan8") != 1.3f && PlayerPrefs.GetFloat("zan8") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1060price);  // PRICE OF 1 CARD
					posit[7] = Instantiate(gtx1060, new Vector3(2.2f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan8", 1.1f);
					
					return;
				}
			}
				break;

			case"Select2card":
			click.Play();

			if(PlayerPrefs.GetFloat("baldol") < gtx1080price)  // PRICE OF 2 CARD
			{
				inf.text = "You have no money!";
			}
			else
			{
				if(PlayerPrefs.GetFloat("zan1") != 1.1f && PlayerPrefs.GetFloat("zan1") != 1.2f && PlayerPrefs.GetFloat("zan1") != 1.3f && PlayerPrefs.GetFloat("zan1") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1080price);   // PRICE OF 2 CARD
					posit[0] = Instantiate(gtx1080, new Vector3(-1.1f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan1", 1.2f);
					
					return;
					
				}
				if(PlayerPrefs.GetFloat("zan2") != 1.1f && PlayerPrefs.GetFloat("zan2") != 1.2f && PlayerPrefs.GetFloat("zan2") != 1.3f && PlayerPrefs.GetFloat("zan2") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1080price);   // PRICE OF 2 CARD
					posit[1] = Instantiate(gtx1080, new Vector3(0f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan2", 1.2f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan3") != 1.1f && PlayerPrefs.GetFloat("zan3") != 1.2f && PlayerPrefs.GetFloat("zan3") != 1.3f && PlayerPrefs.GetFloat("zan3") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1080price);  // PRICE OF 2 CARD
					posit[2] = Instantiate(gtx1080, new Vector3(1.1f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan3", 1.2f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan4") != 1.1f && PlayerPrefs.GetFloat("zan4") != 1.2f && PlayerPrefs.GetFloat("zan4") != 1.3f && PlayerPrefs.GetFloat("zan4") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1080price);  // PRICE OF 2 CARD
					posit[3] = Instantiate(gtx1080, new Vector3(2.2f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan4", 1.2f);
					
					return;
				}
				// VTOROY RYAD

				if(PlayerPrefs.GetFloat("zan5") != 1.1f && PlayerPrefs.GetFloat("zan5") != 1.2f && PlayerPrefs.GetFloat("zan5") != 1.3f && PlayerPrefs.GetFloat("zan5") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1080price);  // PRICE OF 2 CARD
					posit[4] = Instantiate(gtx1080, new Vector3(-1.1f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan5", 1.2f);
					
					return;
					
				}
				if(PlayerPrefs.GetFloat("zan6") != 1.1f && PlayerPrefs.GetFloat("zan6") != 1.2f && PlayerPrefs.GetFloat("zan6") != 1.3f && PlayerPrefs.GetFloat("zan6") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1080price);  // PRICE OF 2 CARD
					posit[5] = Instantiate(gtx1080, new Vector3(0f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan6", 1.2f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan7") != 1.1f && PlayerPrefs.GetFloat("zan7") != 1.2f && PlayerPrefs.GetFloat("zan7") != 1.3f && PlayerPrefs.GetFloat("zan7") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1080price);  // PRICE OF 2 CARD
					posit[6] = Instantiate(gtx1080, new Vector3(1.1f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan7", 1.2f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan8") != 1.1f && PlayerPrefs.GetFloat("zan8") != 1.2f && PlayerPrefs.GetFloat("zan8") != 1.3f && PlayerPrefs.GetFloat("zan8") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - gtx1080price);  // PRICE OF 2 CARD
					posit[7] = Instantiate(gtx1080, new Vector3(2.2f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan8", 1.2f);
					
					return;
				}
			}
				break;

			case"Select3card":
			click.Play();

			if(PlayerPrefs.GetFloat("baldol") < asicprice)  // PRICE OF 3 CARD
			{
				inf.text = "You have no money!";
			}
			else
			{
				if(PlayerPrefs.GetFloat("zan1") != 1.1f && PlayerPrefs.GetFloat("zan1") != 1.2f && PlayerPrefs.GetFloat("zan1") != 1.3f && PlayerPrefs.GetFloat("zan1") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - asicprice); // PRICE OF 3 CARD
					posit[0] = Instantiate(asic, new Vector3(-1.1f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan1", 1.3f);
					
					return;
					
				}
				if(PlayerPrefs.GetFloat("zan2") != 1.1f && PlayerPrefs.GetFloat("zan2") != 1.2f && PlayerPrefs.GetFloat("zan2") != 1.3f && PlayerPrefs.GetFloat("zan2") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - asicprice); // PRICE OF 3 CARD
					posit[1] = Instantiate(asic, new Vector3(0f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan2", 1.3f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan3") != 1.1f && PlayerPrefs.GetFloat("zan3") != 1.2f && PlayerPrefs.GetFloat("zan3") != 1.3f && PlayerPrefs.GetFloat("zan3") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - asicprice); // PRICE OF 3 CARD
					posit[2] = Instantiate(asic, new Vector3(1.1f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan3", 1.3f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan4") != 1.1f && PlayerPrefs.GetFloat("zan4") != 1.2f && PlayerPrefs.GetFloat("zan4") != 1.3f && PlayerPrefs.GetFloat("zan4") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - asicprice); // PRICE OF 3 CARD
					posit[3] = Instantiate(asic, new Vector3(2.2f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan4", 1.3f);
					
					return;
				}
				// VTOROY RYAD

				if(PlayerPrefs.GetFloat("zan5") != 1.1f && PlayerPrefs.GetFloat("zan5") != 1.2f && PlayerPrefs.GetFloat("zan5") != 1.3f && PlayerPrefs.GetFloat("zan5") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - asicprice); // PRICE OF 3 CARD
					posit[4] = Instantiate(asic, new Vector3(-1.1f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan5", 1.3f);
					
					return;
					
				}
				if(PlayerPrefs.GetFloat("zan6") != 1.1f && PlayerPrefs.GetFloat("zan6") != 1.2f && PlayerPrefs.GetFloat("zan6") != 1.3f && PlayerPrefs.GetFloat("zan6") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - asicprice); // PRICE OF 3 CARD
					posit[5] = Instantiate(asic, new Vector3(0f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan6", 1.3f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan7") != 1.1f && PlayerPrefs.GetFloat("zan7") != 1.2f && PlayerPrefs.GetFloat("zan7") != 1.3f && PlayerPrefs.GetFloat("zan7") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - asicprice); // PRICE OF 3 CARD
					posit[6] = Instantiate(asic, new Vector3(1.1f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan7", 1.3f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan8") != 1.1f && PlayerPrefs.GetFloat("zan8") != 1.2f && PlayerPrefs.GetFloat("zan8") != 1.3f && PlayerPrefs.GetFloat("zan8") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - asicprice); // PRICE OF 3 CARD
					posit[7] = Instantiate(asic, new Vector3(2.2f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan8", 1.3f);
					
					return;
				}
			}
				break;

			case"Select4card":
			click.Play();

			if(PlayerPrefs.GetFloat("baldol") < newcardprice) 
			{
				inf.text = "You have no money!";
			}
			else
			{
				if(PlayerPrefs.GetFloat("zan1") != 1.1f && PlayerPrefs.GetFloat("zan1") != 1.2f && PlayerPrefs.GetFloat("zan1") != 1.3f && PlayerPrefs.GetFloat("zan1") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - newcardprice); 
					posit[0] = Instantiate(new_card, new Vector3(-1.1f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan1", 1.4f);
					
					return;
					
				}
				if(PlayerPrefs.GetFloat("zan2") != 1.1f && PlayerPrefs.GetFloat("zan2") != 1.2f && PlayerPrefs.GetFloat("zan2") != 1.3f && PlayerPrefs.GetFloat("zan2") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - newcardprice); 
					posit[1] = Instantiate(new_card, new Vector3(0f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan2", 1.4f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan3") != 1.1f && PlayerPrefs.GetFloat("zan3") != 1.2f && PlayerPrefs.GetFloat("zan3") != 1.3f && PlayerPrefs.GetFloat("zan3") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - newcardprice); 
					posit[2] = Instantiate(new_card, new Vector3(1.1f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan3", 1.4f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan4") != 1.1f && PlayerPrefs.GetFloat("zan4") != 1.2f && PlayerPrefs.GetFloat("zan4") != 1.3f && PlayerPrefs.GetFloat("zan4") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - newcardprice);
					posit[3] = Instantiate(new_card, new Vector3(2.2f, -1.05f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan4", 1.4f);
					
					return;
				}
				// VTOROY RYAD

				if(PlayerPrefs.GetFloat("zan5") != 1.1f && PlayerPrefs.GetFloat("zan5") != 1.2f && PlayerPrefs.GetFloat("zan5") != 1.3f && PlayerPrefs.GetFloat("zan5") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - newcardprice); 
					posit[4] = Instantiate(new_card, new Vector3(-1.1f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan5", 1.4f);
					
					return;
					
				}
				if(PlayerPrefs.GetFloat("zan6") != 1.1f && PlayerPrefs.GetFloat("zan6") != 1.2f && PlayerPrefs.GetFloat("zan6") != 1.3f && PlayerPrefs.GetFloat("zan6") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - newcardprice); 
					posit[5] = Instantiate(new_card, new Vector3(0f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan6", 1.4f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan7") != 1.1f && PlayerPrefs.GetFloat("zan7") != 1.2f && PlayerPrefs.GetFloat("zan7") != 1.3f && PlayerPrefs.GetFloat("zan7") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - newcardprice); 
					posit[6] = Instantiate(new_card, new Vector3(1.1f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan7", 1.4f);
					
					return;
				}
				if(PlayerPrefs.GetFloat("zan8") != 1.1f && PlayerPrefs.GetFloat("zan8") != 1.2f && PlayerPrefs.GetFloat("zan8") != 1.3f && PlayerPrefs.GetFloat("zan8") != 1.4f)
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - newcardprice); 
					posit[7] = Instantiate(new_card, new Vector3(2.2f, -2.15f, 19f), Quaternion.identity);
					PlayerPrefs.SetFloat("zan8", 1.4f);
					return;
				}
			}
				break;
		}
	}

	void Update()
	{
		
	
	}
}
