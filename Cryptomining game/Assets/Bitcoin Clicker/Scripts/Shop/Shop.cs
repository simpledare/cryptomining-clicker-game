﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour {

	public Image close, sel1, sel2, sel3, sel4; // KNOPKI
	public Text inf, set1, set2, set3, set4;
	public Image conf1, conf2, conf3, conf4; // GALOCHKI
	public GameObject forest, city, blue, bigcity;
	public GameObject[] cards = new GameObject[3];
	public Canvas canvasShop;
	public AudioSource click, buy;
	private int priceof2 = 400; // PRICE OF 2 BACKGROUND
	private int priceof3 = 2000; // PRICE OF 3 BACKGROUND
	private int priceof4 = 4000; // PRICE OF 4 BACKGROUND
	private string priceof2str = "400$"; // PRICE OF 2 BACKGROUND
	private string priceof3str = "2000$"; // PRICE OF 3 BACKGROUND
	private string priceof4str = "4000$"; // PRICE OF 4 BACKGROUND


	void Start()
	{
		inf.text = "";
	}
	void OnMouseDown()
	{
		switch(gameObject.name)
		{
			case "Close":
			close.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
				break;

			case "Select1":
			sel1.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
				break;
			
			case "Select2":
			sel2.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
				break;

			case "Select3":
			sel3.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
				break;

			case "Select4":
			sel4.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
				break;
		}
	}

	void OnMouseUp()
	{
		switch(gameObject.name)
		{
			case "Close":
			close.transform.localScale = new Vector3(1f, 1f, 1f);
				break;

			case "Select1":
			sel1.transform.localScale = new Vector3(1f, 1f, 1f);
				break;

			case "Select2":
			sel2.transform.localScale = new Vector3(1f, 1f, 1f);
				break;

			case "Select3":
			sel3.transform.localScale = new Vector3(1f, 1f, 1f);
				break;

			case "Select4":
			sel4.transform.localScale = new Vector3(1f, 1f, 1f);
				break;
		}
	}

	void OnMouseUpAsButton()
	{
		switch(gameObject.name)
		{
			case "Close":
			click.Play();
			inf.text = "";
			
			canvasShop.gameObject.SetActive(false);
			close.transform.localScale = new Vector3(1f, 1f, 1f);
				break;

			case "Select1":
			click.Play();
	
			set2.gameObject.SetActive(true);
			set3.gameObject.SetActive(true);
			set4.gameObject.SetActive(true);

			set1.gameObject.SetActive(false);

			conf2.gameObject.SetActive(false);
			conf3.gameObject.SetActive(false);
			conf4.gameObject.SetActive(false);

			conf1.gameObject.SetActive(true);

			bigcity.gameObject.SetActive(false);
			blue.gameObject.SetActive(false);
			city.gameObject.SetActive(false);

			forest.gameObject.SetActive(true);

			PlayerPrefs.SetFloat("is1", 1f);

			PlayerPrefs.SetFloat("is2", 0f);
			PlayerPrefs.SetFloat("is3", 0f);
			PlayerPrefs.SetFloat("is4", 0f);
				break;

			case "Select2":  // CITY
			click.Play();
			if(PlayerPrefs.GetFloat("ib2") == 1)
			{	
				set1.gameObject.SetActive(true);
				set3.gameObject.SetActive(true);
				set4.gameObject.SetActive(true);

				set2.gameObject.SetActive(false);

				forest.gameObject.SetActive(false);
				bigcity.gameObject.SetActive(false);
				blue.gameObject.SetActive(false);
				
				city.gameObject.SetActive(true);
				
				conf1.gameObject.SetActive(false);
				conf3.gameObject.SetActive(false);
				conf4.gameObject.SetActive(false);

				conf2.gameObject.SetActive(true);

				PlayerPrefs.SetFloat("is2", 1f);

				PlayerPrefs.SetFloat("is1", 0f);
				PlayerPrefs.SetFloat("is3", 0f);
				PlayerPrefs.SetFloat("is4", 0f);
			}
			if(PlayerPrefs.GetFloat("ib2") != 1)
			{
				if(PlayerPrefs.GetFloat("baldol") < priceof2) // PRICE OF 2 BACKGROUND
				{
					inf.text = "You have no money!";
				}
				else
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - priceof2); // PRICE OF 2 BACKGROUND
					PlayerPrefs.SetFloat("ib2", 1f);
					set2.text = "CITY";
					conf2.gameObject.SetActive(true);

					set1.gameObject.SetActive(true);
					set3.gameObject.SetActive(true);
					set4.gameObject.SetActive(true);

					set2.gameObject.SetActive(false);

					forest.gameObject.SetActive(false);
					bigcity.gameObject.SetActive(false);
					blue.gameObject.SetActive(false);
					city.gameObject.SetActive(true);

					conf1.gameObject.SetActive(false);
					conf3.gameObject.SetActive(false);
					conf4.gameObject.SetActive(false);

					conf2.gameObject.SetActive(true);

					PlayerPrefs.SetFloat("is2", 1f);

					PlayerPrefs.SetFloat("is1", 0f);
					PlayerPrefs.SetFloat("is3", 0f);
					PlayerPrefs.SetFloat("is4", 0f);
				}
			}
				break;

			case "Select3":	// WATER
			click.Play();
			if(PlayerPrefs.GetFloat("ib3") == 1)
			{	
				set1.gameObject.SetActive(true);
				set2.gameObject.SetActive(true);
				set4.gameObject.SetActive(true);

				set3.gameObject.SetActive(false);

				forest.gameObject.SetActive(false);
				bigcity.gameObject.SetActive(false);
				city.gameObject.SetActive(false);
				
				blue.gameObject.SetActive(true);
				
				conf1.gameObject.SetActive(false);
				conf2.gameObject.SetActive(false);
				conf4.gameObject.SetActive(false);

				conf3.gameObject.SetActive(true);

				PlayerPrefs.SetFloat("is3", 1f);

				PlayerPrefs.SetFloat("is2", 0f);
				PlayerPrefs.SetFloat("is1", 0f);
				PlayerPrefs.SetFloat("is4", 0f);
			}
			if(PlayerPrefs.GetFloat("ib3") != 1)
			{
				if(PlayerPrefs.GetFloat("baldol") < priceof3) // PRICE OF 3 BACKGROUND
				{
					inf.text = "You have no money!";
				}
				else
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - priceof3); // PRICE OF 3 BACKGROUND
					PlayerPrefs.SetFloat("ib3", 1f);
					set3.text = "WATER";
					set3.gameObject.SetActive(false);
					conf3.gameObject.SetActive(true);

					set1.gameObject.SetActive(true);
					set2.gameObject.SetActive(true);
					set4.gameObject.SetActive(true);

					set3.gameObject.SetActive(false);

					forest.gameObject.SetActive(false);
					bigcity.gameObject.SetActive(false);
					city.gameObject.SetActive(false);

					blue.gameObject.SetActive(true);

					conf1.gameObject.SetActive(false);
					conf2.gameObject.SetActive(false);
					conf4.gameObject.SetActive(false);

					conf3.gameObject.SetActive(true);

					PlayerPrefs.SetFloat("is3", 1f);

					PlayerPrefs.SetFloat("is2", 0f);
					PlayerPrefs.SetFloat("is1", 0f);
					PlayerPrefs.SetFloat("is4", 0f);
				}
			}

				break;

			case "Select4":		// CITY+
			click.Play();
			if(PlayerPrefs.GetFloat("ib4") == 1)
			{	
				set1.gameObject.SetActive(true);
				set2.gameObject.SetActive(true);
				set3.gameObject.SetActive(true);

				set4.gameObject.SetActive(false);

				forest.gameObject.SetActive(false);
				blue.gameObject.SetActive(false);
				city.gameObject.SetActive(false);
				
				bigcity.gameObject.SetActive(true);
				
				conf1.gameObject.SetActive(false);
				conf2.gameObject.SetActive(false);
				conf3.gameObject.SetActive(false);

				conf4.gameObject.SetActive(true);

				PlayerPrefs.SetFloat("is4", 1f);

				PlayerPrefs.SetFloat("is2", 0f);
				PlayerPrefs.SetFloat("is3", 0f);
				PlayerPrefs.SetFloat("is1", 0f);
			}
			if(PlayerPrefs.GetFloat("ib4") != 1)
			{
				if(PlayerPrefs.GetFloat("baldol") < priceof4) // PRICE OF 4 BACKGROUND
				{
					inf.text = "You have no money!";
				}
				else
				{
					buy.Play();
					PlayerPrefs.SetFloat("baldol", PlayerPrefs.GetFloat("baldol") - priceof4); // PRICE OF 4 BACKGROUND
					PlayerPrefs.SetFloat("ib4", 1f);
					set4.text = "CITY+";
					set4.gameObject.SetActive(false);
					conf4.gameObject.SetActive(true);

					set1.gameObject.SetActive(true);
					set2.gameObject.SetActive(true);
					set3.gameObject.SetActive(true);

					set4.gameObject.SetActive(false);

					forest.gameObject.SetActive(false);
					blue.gameObject.SetActive(false);
					city.gameObject.SetActive(false);

					bigcity.gameObject.SetActive(true);

					conf1.gameObject.SetActive(false);
					conf2.gameObject.SetActive(false);
					conf3.gameObject.SetActive(false);

					conf4.gameObject.SetActive(true);

					PlayerPrefs.SetFloat("is4", 1f);

					PlayerPrefs.SetFloat("is2", 0f);
					PlayerPrefs.SetFloat("is3", 0f);
					PlayerPrefs.SetFloat("is1", 0f);
				}
			}
				break;
		}
	}

	void Update()
	{
		if(conf1.gameObject.active == false)
		{
			sel1.gameObject.SetActive(true);
			set1.text = "FOREST";
		}

		if(conf1.gameObject.active == true)
		{
			sel1.gameObject.SetActive(true);
			set1.text = "";
		}





		if(conf2.gameObject.active == false)
		{
			sel2.gameObject.SetActive(true);
			set2.text = "CITY";
		}

		if(conf2.gameObject.active == true)
		{
			sel2.gameObject.SetActive(true);
			set2.text = "";
		}




		if(conf3.gameObject.active == false)
		{
			sel3.gameObject.SetActive(true);
			set3.text = "WATER";
		}

		if(conf3.gameObject.active == true)
		{
			sel3.gameObject.SetActive(true);
			set3.text = "";
		}




		if(conf4.gameObject.active == false)
		{
			sel4.gameObject.SetActive(true);
			set4.text = "CITY+";
		}

		if(conf4.gameObject.active == true)
		{
			sel4.gameObject.SetActive(true);
			set4.text = "";
		}




		if(PlayerPrefs.GetFloat("ib2") == 1 && conf2.gameObject.active == false)
		{
			set2.text = "CITY";
		}
		if(PlayerPrefs.GetFloat("ib2") != 1)
		{
			set2.text = priceof2str;
			conf2.gameObject.SetActive(false);
		}
		if(PlayerPrefs.GetFloat("ib2") == 1 && conf2.gameObject.active == true)
		{
			set2.text = "";
		}



		if(PlayerPrefs.GetFloat("ib3") == 1 && conf3.gameObject.active == false)
		{
			set3.text = "WATER";
		}
		if(PlayerPrefs.GetFloat("ib3") != 1)
		{
			set3.text = priceof3str;
			conf3.gameObject.SetActive(false);
		}
		if(PlayerPrefs.GetFloat("ib3") == 1 && conf3.gameObject.active == true)
		{
			set3.text = "";
		}



		if(PlayerPrefs.GetFloat("ib4") == 1 && conf4.gameObject.active == false)
		{
			set4.text = "CITY+";
		}
		if(PlayerPrefs.GetFloat("ib4") != 1)
		{
			set4.text = priceof4str;
			conf4.gameObject.SetActive(false);
		}
		if(PlayerPrefs.GetFloat("ib4") == 1 && conf4.gameObject.active == true)
		{
			set4.text = "";
		}
	}
}
